package com.example.pokemonapp.utils;

public class FibonacciUtils {

    public static String getNextFibonacciNickname(String nickname, int renameCount) {
        return nickname + "-" + fibonacci(renameCount);
    }

    private static int fibonacci(int n) {
        if (n <= 1) return n;
        int fib = 1;
        int prevFib = 1;

        for (int i = 2; i < n; i++) {
            int temp = fib;
            fib += prevFib;
            prevFib = temp;
        }
        return fib;
    }
}
