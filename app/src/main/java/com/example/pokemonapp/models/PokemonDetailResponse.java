package com.example.pokemonapp.models;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class PokemonDetailResponse {

    @SerializedName("name")
    private String name;

    @SerializedName("sprites")
    private Sprites sprites;

    @SerializedName("moves")
    private List<Move> moves;

    @SerializedName("types")
    private List<Type> types;

    public String getName() {
        return name;
    }

    public Sprites getSprites() {
        return sprites;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public List<Type> getTypes() {
        return types;
    }

    public static class Sprites {
        @SerializedName("front_default")
        private String frontDefault;

        public String getFrontDefault() {
            return frontDefault;
        }
    }

    public static class Move {
        @SerializedName("move")
        private MoveDetail moveDetail;

        public MoveDetail getMoveDetail() {
            return moveDetail;
        }

        public static class MoveDetail {
            @SerializedName("name")
            private String name;

            public String getName() {
                return name;
            }
        }
    }

    public static class Type {
        @SerializedName("type")
        private TypeDetail typeDetail;

        public TypeDetail getTypeDetail() {
            return typeDetail;
        }

        public static class TypeDetail {
            @SerializedName("name")
            private String name;

            public String getName() {
                return name;
            }
        }
    }
}