package com.example.pokemonapp.network;

import com.example.pokemonapp.models.Pokemon;
import com.example.pokemonapp.models.PokemonDetailResponse;
import com.example.pokemonapp.models.PokemonListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("pokemon")
    Call<PokemonListResponse> getPokemonList();

    @GET("pokemon/{name}")
    Call<PokemonDetailResponse> getPokemonDetails(@Path("name") String name);
}
