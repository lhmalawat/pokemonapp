package com.example.pokemonapp.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String POKEAPI_BASE_URL = "https://pokeapi.co/api/v2/";
    private static final String LOCAL_BASE_URL = "http://10.0.2.2:8080/"; // For Android Emulator

    private static Retrofit pokeapiRetrofit = null;
    private static Retrofit localRetrofit = null;

    public static Retrofit getPokeapiClient() {
        if (pokeapiRetrofit == null) {
            pokeapiRetrofit = new Retrofit.Builder()
                    .baseUrl(POKEAPI_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return pokeapiRetrofit;
    }

    public static Retrofit getLocalClient() {
        if (localRetrofit == null) {
            localRetrofit = new Retrofit.Builder()
                    .baseUrl(LOCAL_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return localRetrofit;
    }
}
