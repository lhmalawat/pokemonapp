package com.example.pokemonapp.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LocalApiService {

    @GET("/api/catch")
    Call<Boolean> catchPokemon();

    @GET("/api/release")
    Call<Integer> releasePokemon();

    @POST("/api/rename")
    Call<String> renamePokemon(@Query("name") String name, @Query("sequence") int sequence);
}
