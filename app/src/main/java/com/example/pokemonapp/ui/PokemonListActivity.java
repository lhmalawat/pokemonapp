package com.example.pokemonapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pokemonapp.R;
import com.example.pokemonapp.adapters.PokemonListAdapter;
import com.example.pokemonapp.models.Pokemon;
import com.example.pokemonapp.models.PokemonListResponse;
import com.example.pokemonapp.network.ApiClient;
import com.example.pokemonapp.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class PokemonListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PokemonListAdapter adapter;
    private List<Pokemon> pokemonList = new ArrayList<>();
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        apiService = ApiClient.getPokeapiClient().create(ApiService.class);
        loadPokemonList();
    }

    private void loadPokemonList() {
        Call<PokemonListResponse> call = apiService.getPokemonList();
        call.enqueue(new Callback<PokemonListResponse>() {
            @Override
            public void onResponse(Call<PokemonListResponse> call, Response<PokemonListResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    for (PokemonListResponse.Result result : response.body().getResults()) {
                        String imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + extractIdFromUrl(result.getUrl()) + ".png";
                        pokemonList.add(new Pokemon(result.getName(), imageUrl));
                    }
                    adapter = new PokemonListAdapter(pokemonList, pokemon -> {
                        Intent intent = new Intent(PokemonListActivity.this, PokemonDetailActivity.class);
                        intent.putExtra("pokemon_name", pokemon.getName());
                        startActivity(intent);
                    });
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<PokemonListResponse> call, Throwable t) {
                Log.e("PokemonListActivity", "Error loading Pokemon list", t);
            }
        });
    }

    private int extractIdFromUrl(String url) {
        String[] segments = url.split("/");
        return Integer.parseInt(segments[segments.length - 1]);
    }
}
