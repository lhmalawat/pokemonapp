package com.example.pokemonapp.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.pokemonapp.R;
import com.example.pokemonapp.models.Pokemon;
import com.example.pokemonapp.models.PokemonDetailResponse;
import com.example.pokemonapp.network.ApiClient;
import com.example.pokemonapp.network.ApiService;
import com.example.pokemonapp.network.LocalApiService;
import com.example.pokemonapp.network.PokeapiService;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonDetailActivity extends AppCompatActivity {

    private ImageView pokemonImage;
    private TextView pokemonName;
    private TextView pokemonDetails;
    private Button catchButton;
    private PokeapiService pokeApiService;
    private LocalApiService localApiService;
    private Pokemon currentPokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detail);

        pokemonImage = findViewById(R.id.pokemon_image);
        pokemonName = findViewById(R.id.pokemon_name);
        pokemonDetails = findViewById(R.id.pokemon_details);
        catchButton = findViewById(R.id.catch_button);

        pokeApiService = ApiClient.getPokeapiClient().create(PokeapiService.class);
        localApiService = ApiClient.getLocalClient().create(LocalApiService.class);
        String pokemonName = getIntent().getStringExtra("pokemon_name");
        loadPokemonDetails(pokemonName);

        catchButton.setOnClickListener(v -> {
            Log.d("PokemonDetailActivity", "Catch button clicked");
            catchPokemon();
        });
    }

    private void loadPokemonDetails(String name) {
        Call<PokemonDetailResponse> call = pokeApiService.getPokemonDetails(name);
        call.enqueue(new Callback<PokemonDetailResponse>() {
            @Override
            public void onResponse(Call<PokemonDetailResponse> call, Response<PokemonDetailResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    displayPokemonDetails(response.body());
                } else {
                    Toast.makeText(PokemonDetailActivity.this, "Failed to load Pokemon details", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PokemonDetailResponse> call, Throwable t) {
                Toast.makeText(PokemonDetailActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayPokemonDetails(PokemonDetailResponse pokemonDetailResponse) {
        currentPokemon = new Pokemon(pokemonDetailResponse.getName(), pokemonDetailResponse.getSprites().getFrontDefault());
        pokemonName.setText(currentPokemon.getName());
        Picasso.get().load(currentPokemon.getImageUrl()).into(pokemonImage);

        StringBuilder moves = new StringBuilder();
        for (PokemonDetailResponse.Move move : pokemonDetailResponse.getMoves()) {
            moves.append(move.getMoveDetail().getName()).append(", ");
        }

        StringBuilder types = new StringBuilder();
        for (PokemonDetailResponse.Type type : pokemonDetailResponse.getTypes()) {
            types.append(type.getTypeDetail().getName()).append(", ");
        }

        pokemonDetails.setText("Moves: " + moves.toString() + "\nTypes: " + types.toString());
    }

    private void catchPokemon() {
        Log.d("PokemonDetailActivity", "Attempting to catch Pokemon");
        Call<Boolean> call = localApiService.catchPokemon();
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.body() != null && response.body()) {
                    promptForNickname();
                } else {
                    Toast.makeText(PokemonDetailActivity.this, "Failed to catch Pokemon!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("PokemonDetailActivity", "Error catching Pokemon", t);
            }
        });
    }

    private void promptForNickname() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nickname your Pokemon");

        final EditText input = new EditText(this);
        builder.setView(input);

        builder.setPositiveButton("OK", (dialog, which) -> {
            String nickname = input.getText().toString();
            currentPokemon.setName(nickname);
            savePokemonToMyList();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void savePokemonToMyList() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyPokemonList", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String json = gson.toJson(currentPokemon);

        Set<String> myPokemonSet = sharedPreferences.getStringSet("myPokemonList", new HashSet<>());
        myPokemonSet.add(json);

        editor.putStringSet("myPokemonList", myPokemonSet);
        editor.apply();

        Toast.makeText(PokemonDetailActivity.this, "Pokemon added to My Pokemon List!", Toast.LENGTH_SHORT).show();
    }
}
