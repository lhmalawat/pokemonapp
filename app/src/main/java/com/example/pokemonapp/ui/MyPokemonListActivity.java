package com.example.pokemonapp.ui;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pokemonapp.R;
import com.example.pokemonapp.adapters.MyPokemonListAdapter;
import com.example.pokemonapp.models.Pokemon;
import com.example.pokemonapp.network.ApiClient;
import com.example.pokemonapp.network.LocalApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPokemonListActivity extends AppCompatActivity {

    public static List<Pokemon> myPokemonList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MyPokemonListAdapter adapter;
    private LocalApiService localApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pokemon_list);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        localApiService = ApiClient.getLocalClient().create(LocalApiService.class);

        adapter = new MyPokemonListAdapter(myPokemonList, new MyPokemonListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Pokemon pokemon) {
                // Handle item click
            }

            @Override
            public void onReleaseClick(Pokemon pokemon) {
                releasePokemon(pokemon);
            }

            @Override
            public void onRenameClick(Pokemon pokemon) {
                renamePokemon(pokemon);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void releasePokemon(Pokemon pokemon) {
        Call<Integer> call = localApiService.releasePokemon();
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.body() != null && response.body() != -1) {
                    Toast.makeText(MyPokemonListActivity.this, "Pokemon released!", Toast.LENGTH_SHORT).show();
                    adapter.removePokemon(pokemon); // Update the adapter
                } else {
                    Toast.makeText(MyPokemonListActivity.this, "Failed to release Pokemon!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(MyPokemonListActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void renamePokemon(Pokemon pokemon) {
        int renameCount = getRenameCount(pokemon); // Implement this method to track renaming
        Call<String> call = localApiService.renamePokemon(pokemon.getName(), renameCount);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() != null) {
                    pokemon.setName(response.body());
                    adapter.updatePokemon(pokemon);
                    Toast.makeText(MyPokemonListActivity.this, "Pokemon renamed to " + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Handle failure
                Toast.makeText(MyPokemonListActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int getRenameCount(Pokemon pokemon) {
        return 1;
    }
}
