package com.example.pokemonapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pokemonapp.ui.MyPokemonListActivity;
import com.example.pokemonapp.ui.PokemonListActivity;

public class MainActivity extends AppCompatActivity {

    private Button buttonPokemonList;
    private Button buttonMyPokemonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonPokemonList = findViewById(R.id.button_pokemon_list);
        buttonMyPokemonList = findViewById(R.id.button_my_pokemon_list);

        buttonPokemonList.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, PokemonListActivity.class);
            startActivity(intent);
        });

        buttonMyPokemonList.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, MyPokemonListActivity.class);
            startActivity(intent);
        });
    }
}