package com.example.pokemonapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pokemonapp.R;
import com.example.pokemonapp.models.Pokemon;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MyPokemonListAdapter extends RecyclerView.Adapter<MyPokemonListAdapter.ViewHolder> {

    private List<Pokemon> pokemonList;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Pokemon pokemon);
        void onReleaseClick(Pokemon pokemon);
        void onRenameClick(Pokemon pokemon);
    }

    public MyPokemonListAdapter(List<Pokemon> pokemonList, OnItemClickListener listener) {
        this.pokemonList = pokemonList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pokemon pokemon = pokemonList.get(position);
        holder.bind(pokemon, listener);
    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }

    public void removePokemon(Pokemon pokemon) {
        int position = pokemonList.indexOf(pokemon);
        if (position != -1) {
            pokemonList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void updatePokemon(Pokemon pokemon) {
        int position = pokemonList.indexOf(pokemon);
        if (position != -1) {
            pokemonList.set(position, pokemon);
            notifyItemChanged(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView pokemonName;
        public ImageView pokemonImage;
        public Button releaseButton;
        public Button renameButton;

        public ViewHolder(View itemView) {
            super(itemView);
            pokemonName = itemView.findViewById(R.id.pokemon_name);
            pokemonImage = itemView.findViewById(R.id.pokemon_image);
            releaseButton = itemView.findViewById(R.id.release_button);
            renameButton = itemView.findViewById(R.id.rename_button);
        }

        public void bind(final Pokemon pokemon, final OnItemClickListener listener) {
            pokemonName.setText(pokemon.getName());
            Picasso.get().load(pokemon.getImageUrl()).into(pokemonImage);
            itemView.setOnClickListener(v -> listener.onItemClick(pokemon));
            releaseButton.setOnClickListener(v -> listener.onReleaseClick(pokemon));
            renameButton.setOnClickListener(v -> listener.onRenameClick(pokemon));
        }
    }
}
